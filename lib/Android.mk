LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)
LOCAL_MODULE := tef6686test
#LOCAL_LDLIBS := -llog
#LOCAL_LDFLAGS := -static
#LOCAL_FORCE_STATIC_EXECUTABLE := true
LOCAL_SRC_FILES := main.cpp Tuner_Api.cpp Tuner_Api_Lithio.cpp Tuner_Drv_Lithio.cpp Tuner_Interface.cpp Tuner_Proc.cpp
#LOCAL_CFLAGS := -std=gnu11
LOCAL_CFLAGS := -Wno-unused-parameter -Wno-error
#LOCAL_MODULE_PATH := /system/bin/
include $(BUILD_EXECUTABLE)

