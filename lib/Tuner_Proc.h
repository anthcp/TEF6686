#ifndef 	__TUNER_H
#define	__TUNER_H

int Tuner_Power_on(void);
void tune(int);
int seek(int);
void tune_direct(uint16_t, int);

#endif

